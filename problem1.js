const fs = require("fs").promises;

const directory = './JSONDirectory'

function createAndDeleteJSONFiles(numberOfFiles) {
  if (typeof numberOfFiles === "number") {
    fs.mkdir(directory,{recursive:true})
        .then(() => {
        console.log("directory created");
        let count = 1;
        function res(count) {
            if (count <= numberOfFiles) {
            let fileName = `file${count}.json`;
            let filePath = `${directory}${fileName}`
            let fileData = {
                id: count,
                data: `file${count}`,
            };
            fs.writeFile(filePath, JSON.stringify(fileData))
                .then(() => {
                    console.log(`${fileName} created sucessfully`);
                    return fs.unlink(filePath);
                })
                .then(() => {
                    console.log(`${fileName} deleted sucessfully`);
                    res(++count);
                });
            }
        }
        res(count);
      })
      .catch((err) => {
        console.error(err);
      });
  } else {
    console.log(
      "Argument should be a number"
    );
  }
}

module.exports = createAndDeleteJSONFiles;