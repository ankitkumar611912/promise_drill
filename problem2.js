const fs = require("fs").promises;


function implementationOfProblem2() {
  fs.readFile("./lipsum_1.txt", "utf-8")
        .then((data) => {
            let upperCaseData = data.toUpperCase();
            return fs.writeFile("upperCaseFile.txt", upperCaseData);
        })
        .then(() => {
            console.log("Uppercase file written successfully");
            return fs.writeFile("filenames.txt", "upperCaseFile.txt ");
        })
        .then(() => {
            console.log("Uppercase filename written successfully");
            return fs.readFile("upperCaseFile.txt", "utf-8");
        })
        .then((uppercaseData) => {
            let lowerCaseData = uppercaseData.toLowerCase();
            let sentences = lowerCaseData.split(".");
            let newSentences = sentences
                .map((sentence) => {
                    sentence = sentence.trim();
                    return sentence;
                })
                .filter((sentences) => sentences.length !== 0);
            const content = newSentences.join("\n");
            return fs.writeFile("lowerCaseFile.txt", content);
        })
        .then(() => {
            console.log("Lowercase file written successfully");
            return fs.writeFile("filenames.txt", "lowerCaseFile.txt ", {
            flag: "a",
            });
        })
        .then(() => {
            console.log("Lowercase filename written successfully");
            return fs.readFile("lowerCaseFile.txt", "utf-8");
        })
        .then((lowercaseData) => {
            let sentences = lowercaseData.split("\n");
            sentences.sort();
            const content = sentences.join("\n");
            return fs.writeFile("sortedFile.txt", content);
        })
        .then(() => {
            console.log("Sorted file written successfully");
            return fs.writeFile("filenames.txt", "sortedFile.txt", { flag: "a" });
        })
        .then(() => {
            console.log("Sorted filename written successfully");
            return fs.readFile("filenames.txt", "utf-8");
        })
        .then((fileNameData) => {
            let filesArray = fileNameData.split(" ");
            for (let index = 0; index < filesArray.length; index++) {
                const fileName = filesArray[index];
                fs.unlink(fileName).then(() => {
                    console.log(`${fileName} deleted successfully`);
                });
            }
        });
}

implementationOfProblem2();